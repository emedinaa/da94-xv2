-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 11, 2019 at 01:41 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ACADEMICO`
--

-- --------------------------------------------------------

--
-- Table structure for table `estudiante`
--

CREATE TABLE `estudiante` (
  `codalu` int(8) NOT NULL DEFAULT '0',
  `apep` varchar(15) NOT NULL DEFAULT '',
  `apem` varchar(15) NOT NULL DEFAULT '',
  `nom` varchar(25) NOT NULL DEFAULT '',
  `sex` char(1) NOT NULL DEFAULT '',
  `secc` char(1) NOT NULL DEFAULT '',
  `p1` int(2) NOT NULL DEFAULT '0',
  `p2` int(2) NOT NULL DEFAULT '0',
  `p3` int(2) NOT NULL DEFAULT '0',
  `ep` int(2) NOT NULL DEFAULT '0',
  `ef` int(2) NOT NULL DEFAULT '0',
  `trabajo` char(1) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `estudiante`
--

INSERT INTO `estudiante` (`codalu`, `apep`, `apem`, `nom`, `sex`, `secc`, `p1`, `p2`, `p3`, `ep`, `ef`, `trabajo`) VALUES
(18271187, 'ACAPANA', 'VEGA', 'MIGUEL MARTIN', 'M', 'A', 13, 10, 10, 14, 5, 'S'),
(18271267, 'ALVARADO', 'GARGATE', 'ANGELICA KRISTEL', 'F', 'A', 14, 5, 18, 20, 9, 'S'),
(18271347, 'ANTICONA', 'PALOMINO', 'SANDRO CARLOS', 'M', 'C', 7, 11, 15, 9, 15, 'N'),
(18271427, 'APOLAYA', 'GARCIA', 'YANNINA YSABEL', 'F', 'A', 19, 10, 7, 12, 18, 'S'),
(18271697, 'ARANDA', 'DOMINGUEZ', 'JOSE DAVID', 'M', 'B', 12, 15, 17, 18, 9, 'N'),
(18271777, 'ARCE', 'ROMANI', 'ROMEL MAX', 'M', 'C', 10, 16, 9, 16, 9, 'N'),
(18271857, 'ARIADELA', 'CASTANEDA', 'JESSICA ROSA', 'F', 'B', 17, 14, 12, 15, 17, 'S'),
(18271937, 'ARROYO', 'RAMIREZ', 'EFRAIN LUIS', 'M', 'C', 9, 8, 16, 15, 4, 'S'),
(18272077, 'ATTE', 'VELASQUEZ', 'FIORELLA PATRICIA', 'F', 'C', 19, 5, 19, 6, 11, 'N'),
(18272157, 'AVELLANEDA', 'ARRIETA', 'LUIS ANGEL', 'M', 'A', 13, 11, 14, 17, 17, 'S'),
(10000, 'medina', 'alfaro', 'eduardo jose', 'M', 'A', 10, 11, 12, 13, 14, 'S'),
(1233323, 'dddd', 'eee', 'eeee', 'M', 'A', 10, 11, 12, 13, 14, 'N'),
(1233324, 'dddd', 'eee', 'eeee', 'M', 'A', 10, 11, 12, 13, 14, 'N'),
(1233325, 'dddd', 'eee', 'eeee', 'M', 'A', 10, 11, 12, 13, 14, 'N'),
(1213243434, 'sadd', 'swdw', 'wewwee', 'F', 'A', 10, 20, 12, 14, 14, 'S'),
(13534355, 'demo', 'demo', 'demo', 'M', 'A', 11, 11, 11, 11, 11, 'S'),
(12000000, 'Medina', 'alfaro', '', 'S', 'A', 0, 0, 0, 0, 0, 'S'),
(12000001, 'qwwqw', 'qwqwqw', 'qwqde', 'M', 'A', 10, 10, 10, 10, 10, 'S'),
(12000002, 'wewe', 'we', 'wewe', 'M', 'A', 1, 1, 1, 12, 12, 'S'),
(12000010, 'lopez', 'roca', 'diana', 'F', 'A', 12, 14, 13, 12, 15, 'S');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `estudiante`
--
ALTER TABLE `estudiante`
  ADD PRIMARY KEY (`codalu`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
