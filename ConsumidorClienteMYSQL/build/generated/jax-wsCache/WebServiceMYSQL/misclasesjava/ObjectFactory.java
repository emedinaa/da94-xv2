
package misclasesjava;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the misclasesjava package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://MisClasesJava/", "Exception");
    private final static QName _ObtieneID_QNAME = new QName("http://MisClasesJava/", "ObtieneID");
    private final static QName _ObtieneIDResponse_QNAME = new QName("http://MisClasesJava/", "ObtieneIDResponse");
    private final static QName _ObtieneNombre_QNAME = new QName("http://MisClasesJava/", "ObtieneNombre");
    private final static QName _ObtieneNombreResponse_QNAME = new QName("http://MisClasesJava/", "ObtieneNombreResponse");
    private final static QName _AlumnosPorSeccion_QNAME = new QName("http://MisClasesJava/", "alumnosPorSeccion");
    private final static QName _AlumnosPorSeccionResponse_QNAME = new QName("http://MisClasesJava/", "alumnosPorSeccionResponse");
    private final static QName _Hello_QNAME = new QName("http://MisClasesJava/", "hello");
    private final static QName _HelloResponse_QNAME = new QName("http://MisClasesJava/", "helloResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: misclasesjava
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link ObtieneID }
     * 
     */
    public ObtieneID createObtieneID() {
        return new ObtieneID();
    }

    /**
     * Create an instance of {@link ObtieneIDResponse }
     * 
     */
    public ObtieneIDResponse createObtieneIDResponse() {
        return new ObtieneIDResponse();
    }

    /**
     * Create an instance of {@link ObtieneNombre }
     * 
     */
    public ObtieneNombre createObtieneNombre() {
        return new ObtieneNombre();
    }

    /**
     * Create an instance of {@link ObtieneNombreResponse }
     * 
     */
    public ObtieneNombreResponse createObtieneNombreResponse() {
        return new ObtieneNombreResponse();
    }

    /**
     * Create an instance of {@link AlumnosPorSeccion }
     * 
     */
    public AlumnosPorSeccion createAlumnosPorSeccion() {
        return new AlumnosPorSeccion();
    }

    /**
     * Create an instance of {@link AlumnosPorSeccionResponse }
     * 
     */
    public AlumnosPorSeccionResponse createAlumnosPorSeccionResponse() {
        return new AlumnosPorSeccionResponse();
    }

    /**
     * Create an instance of {@link Hello }
     * 
     */
    public Hello createHello() {
        return new Hello();
    }

    /**
     * Create an instance of {@link HelloResponse }
     * 
     */
    public HelloResponse createHelloResponse() {
        return new HelloResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://MisClasesJava/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtieneID }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://MisClasesJava/", name = "ObtieneID")
    public JAXBElement<ObtieneID> createObtieneID(ObtieneID value) {
        return new JAXBElement<ObtieneID>(_ObtieneID_QNAME, ObtieneID.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtieneIDResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://MisClasesJava/", name = "ObtieneIDResponse")
    public JAXBElement<ObtieneIDResponse> createObtieneIDResponse(ObtieneIDResponse value) {
        return new JAXBElement<ObtieneIDResponse>(_ObtieneIDResponse_QNAME, ObtieneIDResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtieneNombre }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://MisClasesJava/", name = "ObtieneNombre")
    public JAXBElement<ObtieneNombre> createObtieneNombre(ObtieneNombre value) {
        return new JAXBElement<ObtieneNombre>(_ObtieneNombre_QNAME, ObtieneNombre.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtieneNombreResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://MisClasesJava/", name = "ObtieneNombreResponse")
    public JAXBElement<ObtieneNombreResponse> createObtieneNombreResponse(ObtieneNombreResponse value) {
        return new JAXBElement<ObtieneNombreResponse>(_ObtieneNombreResponse_QNAME, ObtieneNombreResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlumnosPorSeccion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://MisClasesJava/", name = "alumnosPorSeccion")
    public JAXBElement<AlumnosPorSeccion> createAlumnosPorSeccion(AlumnosPorSeccion value) {
        return new JAXBElement<AlumnosPorSeccion>(_AlumnosPorSeccion_QNAME, AlumnosPorSeccion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlumnosPorSeccionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://MisClasesJava/", name = "alumnosPorSeccionResponse")
    public JAXBElement<AlumnosPorSeccionResponse> createAlumnosPorSeccionResponse(AlumnosPorSeccionResponse value) {
        return new JAXBElement<AlumnosPorSeccionResponse>(_AlumnosPorSeccionResponse_QNAME, AlumnosPorSeccionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Hello }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://MisClasesJava/", name = "hello")
    public JAXBElement<Hello> createHello(Hello value) {
        return new JAXBElement<Hello>(_Hello_QNAME, Hello.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HelloResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://MisClasesJava/", name = "helloResponse")
    public JAXBElement<HelloResponse> createHelloResponse(HelloResponse value) {
        return new JAXBElement<HelloResponse>(_HelloResponse_QNAME, HelloResponse.class, null, value);
    }

}
