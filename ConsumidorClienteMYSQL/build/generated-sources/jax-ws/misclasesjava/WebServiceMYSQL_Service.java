
package misclasesjava;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.11-b150120.1832
 * Generated source version: 2.1
 * 
 */
@WebServiceClient(name = "WebServiceMYSQL", targetNamespace = "http://MisClasesJava/", wsdlLocation = "http://localhost:9090/WebServiceMysql/WebServiceMYSQL?wsdl")
public class WebServiceMYSQL_Service
    extends Service
{

    private final static URL WEBSERVICEMYSQL_WSDL_LOCATION;
    private final static WebServiceException WEBSERVICEMYSQL_EXCEPTION;
    private final static QName WEBSERVICEMYSQL_QNAME = new QName("http://MisClasesJava/", "WebServiceMYSQL");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:9090/WebServiceMysql/WebServiceMYSQL?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        WEBSERVICEMYSQL_WSDL_LOCATION = url;
        WEBSERVICEMYSQL_EXCEPTION = e;
    }

    public WebServiceMYSQL_Service() {
        super(__getWsdlLocation(), WEBSERVICEMYSQL_QNAME);
    }

    public WebServiceMYSQL_Service(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    /**
     * 
     * @return
     *     returns WebServiceMYSQL
     */
    @WebEndpoint(name = "WebServiceMYSQLPort")
    public WebServiceMYSQL getWebServiceMYSQLPort() {
        return super.getPort(new QName("http://MisClasesJava/", "WebServiceMYSQLPort"), WebServiceMYSQL.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns WebServiceMYSQL
     */
    @WebEndpoint(name = "WebServiceMYSQLPort")
    public WebServiceMYSQL getWebServiceMYSQLPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://MisClasesJava/", "WebServiceMYSQLPort"), WebServiceMYSQL.class, features);
    }

    private static URL __getWsdlLocation() {
        if (WEBSERVICEMYSQL_EXCEPTION!= null) {
            throw WEBSERVICEMYSQL_EXCEPTION;
        }
        return WEBSERVICEMYSQL_WSDL_LOCATION;
    }

}
