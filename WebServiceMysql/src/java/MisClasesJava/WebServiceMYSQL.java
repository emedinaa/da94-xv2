/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MisClasesJava;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author emedinaa
 */
@WebService(serviceName = "WebServiceMYSQL")
public class WebServiceMYSQL {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "ObtieneNombre")
    public String ObtieneNombre(@WebParam(name = "codigoCliente") String codigoCliente) 
     throws Exception{
        //TODO write your implementation code here:
        
        ResultSet rs = null;
        String nombreCliente = null;
        ConectaMysql connection = new ConectaMysql();
        rs = connection.Listar("SELECT * FROM estudiante WHERE codalu='"+codigoCliente+"';");
        while (rs.next()) {
            nombreCliente = rs.getString(2)+ " "+rs.getString(3)+ " "+rs.getString(4);
        }
        return nombreCliente;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "ObtieneID")
    public String ObtieneID(@WebParam(name = "nombre") String nombre) throws Exception{
        //TODO write your implementation code here:
        ResultSet rs = null;
        String codigoCliente = null;
        ConectaMysql connection = new ConectaMysql();
        rs = connection.Listar("SELECT * FROM estudiante WHERE apep='"+nombre+"';");
        while (rs.next()) {
            codigoCliente = rs.getString(1);
        }
        return codigoCliente;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "alumnosPorSeccion")
    public List<String> alumnosPorSeccion(@WebParam(name = "seccion") String seccion)throws Exception {
        //TODO write your implementation code here:
        
        ResultSet rs = null;
        List<String> alumnos = new ArrayList<>();
        ConectaMysql connection = new ConectaMysql();
        rs = connection.Listar("SELECT * FROM estudiante WHERE secc='"+seccion+"';");
        while (rs.next()) {
            String alumno = rs.getString(1)+ " "+ rs.getString(2)+ " "+ rs.getString(3);
            alumnos.add(alumno);
        }
        return alumnos;
    }
}
