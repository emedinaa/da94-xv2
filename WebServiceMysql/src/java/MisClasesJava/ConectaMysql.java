/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MisClasesJava;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.DriverManager;

/**
 *
 * @author emedinaa
 */
public class ConectaMysql {

    public Connection getConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://localhost:3306/ACADEMICO";
            return (DriverManager.getConnection(url, "uigv", "uigv"));
        } catch (Exception e) {
            System.out.println("error" + e.getMessage());
        }
        return null;
    }

    public ResultSet Listar(String sql) {
        Statement st = null;
        ResultSet rs = null;
        try {
            Connection conn = this.getConnection();
            st = conn.createStatement();
            rs = st.executeQuery(sql);
        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
        }
        return rs;
    }
}
